import styles from './button.module.scss';
import React from 'react';
import { Button, ButtonProps } from '@material-ui/core';

export function CalcButton(props: ButtonProps) {
  return <Button className={styles.calcButton} {...props} />;
}
