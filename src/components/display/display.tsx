import { TextField } from '@material-ui/core';
import React from 'react';

export function Display(props: CalcButtonProps) {
  return <TextField value={props.value} />;
}

type CalcButtonProps = { value: string };
