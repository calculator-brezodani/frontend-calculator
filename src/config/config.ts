import convict from 'convict';

const convictConfig = convict({
  API_URL: {
    doc: 'Api Url',
    format: String,
    default: 'http://localhost:9100',
    env: 'API_URL',
  },
});

convictConfig.validate({ allowed: 'strict' });
export const config = convictConfig.getProperties();
export type ConfigObject = typeof config;
