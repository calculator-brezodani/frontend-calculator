import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { initAndGetStore } from './state/get-store';
import { appReducer } from './state/reducer';
import * as allEpics from './state/epics';
import { combineEpics } from 'redux-observable';

const rootEpic = combineEpics(...Object.entries(allEpics).map(([_, value]) => value));

ReactDOM.render(
  <Provider store={initAndGetStore({ rootEpic, reducer: appReducer })}>
    <App />
  </Provider>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
