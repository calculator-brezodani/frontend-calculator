import { AppState } from './reducer';

export const appSelectors = {
  result: (state: AppState) => state.result,
  resultLoading: (state: AppState) => state.resultLoading,
};
