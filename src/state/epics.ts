import { Action } from 'redux';
import { filter, mergeMap, map, catchError, tap } from 'rxjs/operators';
import { ActionsObservable } from 'redux-observable';
import { of } from 'rxjs';
import { appActions } from './actions';
import { appRequests } from '../api.service';

export const calculate$ = (actions$: ActionsObservable<Action>) =>
  actions$.pipe(
    filter(appActions.calculateRequest.match),
    mergeMap(({ payload }) =>
      appRequests.calculate(payload.expression).pipe(
        map(({ response }) => appActions.calculateSuccess({ result: response.result })),
        catchError(error => of(appActions.calculateFail({ error }))),
      ),
    ),
  );
