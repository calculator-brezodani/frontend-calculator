import { createReducer } from '@reduxjs/toolkit';
import { appActions } from './actions';

export interface AppState {
  result: string;
  resultLoading: boolean;
}

export const appInitialState: AppState = {
  result: '',
  resultLoading: false,
};

export const appReducer = createReducer(appInitialState, builder =>
  builder
    .addCase(appActions.calculateRequest, state => ({
      ...state,
      resultLoading: true,
    }))
    .addCase(appActions.calculateSuccess, (state, { payload }) => ({
      ...state,
      resultLoading: false,
      result: payload.result,
    }))
    .addCase(appActions.calculateFail, state => ({
      ...state,
      resultLoading: false,
    })),
);
