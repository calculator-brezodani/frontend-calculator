import { createEpicMiddleware, Epic } from 'redux-observable';
import { Reducer, Store, AnyAction, createStore, applyMiddleware } from 'redux';
import { AppState } from './reducer';
import { composeWithDevTools } from 'redux-devtools-extension';
import ReduxPromise from 'redux-promise';

const socketEpicMiddleware = createEpicMiddleware();

export const initAndGetStore = ({
  reducer,
  rootEpic,
}: {
  reducer: Reducer<AppState, AnyAction>;
  rootEpic: Epic;
}): Store<AppState> => {
  const store = createStore(reducer, composeWithDevTools(applyMiddleware(socketEpicMiddleware, ReduxPromise)));
  socketEpicMiddleware.run(rootEpic);
  return store;
};
