import { createAction } from '@reduxjs/toolkit';

export const appActions = {
  calculateRequest: createAction<{ expression: string }>('calculate/Request'),
  calculateSuccess: createAction<{ result: string }>('calculate/Success'),
  calculateFail: createAction<{ error: any }>('calculate/Fail'),
};
