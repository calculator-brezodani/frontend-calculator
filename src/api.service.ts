import { ajax } from 'rxjs/ajax';
import { map } from 'rxjs/operators';
import { config } from './config/config';

const apiUrl = config.API_URL;
const headers = { 'content-type': 'application/json' };

export const post = <R = any, B = any>({ url, body }: { url: string; body: B }) => {
  console.log(body, `${apiUrl}/${url}`);
  return ajax
    .post(`${apiUrl}/${url}`, JSON.stringify(body), headers)
    .pipe(map(ajaxResponse => ({ ...ajaxResponse, response: ajaxResponse.response as R })));
};

export const get = <R = any>(url: string) =>
  ajax
    .get(`${apiUrl}/${url}`, headers)
    .pipe(map(ajaxResponse => ({ ...ajaxResponse, response: ajaxResponse.response as R })));

export const appRequests = {
  calculate: (formula: string) =>
    post<{ result: string }, { formula: string }>({ url: 'calculate', body: { formula } }),
};
