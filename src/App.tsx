import React, { useEffect, useState } from 'react';
import { CalcButton } from './components/button/button';
import styles from './App.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import { appActions } from './state/actions';
import { appSelectors } from './state/selectors';
import { CalculatorButtons } from './shared/utils/CalculatorButtons';
import { Grid } from '@material-ui/core';
import { Display } from './components/display/display';

function App() {
  const result = useSelector(appSelectors.result);
  const dispatch = useDispatch();
  const [data, update] = useState(result);
  const [finalFormula, sendFormula] = useState('');
  const reset = () => update('');

  useEffect(() => {
    dispatch(appActions.calculateRequest({ expression: finalFormula }));
  }, [finalFormula, dispatch]);

  useEffect(() => {
    update(result);
  }, [result, update]);

  return (
    <div className={styles.root}>
      <Display value={data}></Display>
      {CalculatorButtons.map((calculatorButtonRow, rowIndex) => (
        <Grid container direction="row" key={rowIndex}>
          {calculatorButtonRow.map((calculatorButton, columnIndex) => (
            <div key={columnIndex}>
              <CalcButton onClick={() => update(`${data} ${calculatorButton.value}`)}>
                <div className={styles.buttonText}>{calculatorButton.value}</div>
              </CalcButton>
              {rowIndex === CalculatorButtons.length - 1 && columnIndex === calculatorButtonRow.length - 1 && (
                <CalcButton onClick={() => sendFormula(data)} type="submit" name="submit">
                  <div className={styles.buttonText}>=</div>
                </CalcButton>
              )}
            </div>
          ))}
        </Grid>
      ))}
      <CalcButton onClick={reset}>
        <div>Clear</div>
      </CalcButton>
    </div>
  );
}

export default App;
